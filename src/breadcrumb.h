#include <stdint.h>
#define BREADCRUMB_SECTION __attribute__ ((section(".breadcrumb")))

void breadcrumb_write(uint8_t * string);
uint8_t * breadcrumb_getString(void);
