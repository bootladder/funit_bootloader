#include "Commands.h"
#include "System.h"
#include "UART.h"
#include "halFlash.h"
#include "halReset.h"
#include <stdint.h>
//#include <string.h>
#include "my_memcpy.h"
#include "hton.h"
#include "sysTypes.h"

typedef int (*commandHandler)(uint8_t * cmd);
typedef uint32_t (funitPointer)(uint32_t param);

void HAL_LedOn(uint8_t i);//this is macro generated

/* Execute Funit 
 *
 * Command contains address of funit, and uint32_t parameter
 * Return a uint32_t to the host (UART)
 *
 */
int HandlerExecuteFunit(uint8_t* cmd)
{
  UART_WriteByte(5);
  
  uint32_t parsed_addr;
  my_memcpy(&parsed_addr,cmd,4);
  //We are little endian
  parsed_addr = ntohl(parsed_addr);

  if( parsed_addr & 0x000000FF ) {
    UART_WriteByte(2); //invalid
    return 2;
  }

  uint32_t parsed_param;
  my_memcpy(&parsed_param,&cmd[4],4);
  //We are little endian
  parsed_param = ntohl(parsed_param);

  //the executable code starts at byte 16.  starts at odd number? ARM?
  parsed_addr+=17;
  funitPointer * fptr = (funitPointer*) (uintptr_t)  parsed_addr;

  uint32_t ret;
  //Execute the funit
    ATOMIC_SECTION_ENTER
  ret = fptr( parsed_param );
    ATOMIC_SECTION_LEAVE

  //Write Status: OK
  UART_WriteByte(1); //OK

  //Write 4 bytes return value, in network order from little endian
  uint8_t * retptr = (uint8_t *) & ret;
 
  //Network order is MSB first.
  //Little endian means MSB is the highest byte
  UART_WriteByte(retptr[3]); 
  UART_WriteByte(retptr[2]); 
  UART_WriteByte(retptr[1]); 
  UART_WriteByte(retptr[0]); 
    
  return 0;
}

int HandlerReadFlash256(uint8_t * cmd)
{
  UART_WriteByte(4);
  
  uint32_t parsed_addr;
  my_memcpy(&parsed_addr,cmd,4);
  parsed_addr = ntohl(parsed_addr);
  
  if( parsed_addr & 0x000000FF || parsed_addr > 0x40000 ) {
    UART_WriteByte(2); //invalid
    return 2;
  }

  UART_WriteByte(1); //OK

  //loop through the actual flash address space here
  //flash must be accessed by 32-bit word
  uint32_t * baseptr = (uint32_t*)(uintptr_t)parsed_addr; //casting for the compiler
  static uint32_t buf[256/4];
  for(int i=0; i<256/4;i++)
  {
    buf[i] = *(baseptr + i);
  }

  uint8_t * bufbytes = (uint8_t*)buf;
  for(int i=0; i<256;i++)
  {
    UART_WriteByte(bufbytes[i]);
  }
  return 0;
}
int HandlerWriteFlash256(uint8_t* cmd)
{
  UART_WriteByte(3);
  
  uint32_t parsed_addr;
  my_memcpy(&parsed_addr,cmd,4);
  parsed_addr = ntohl(parsed_addr);

  if( parsed_addr & 0x000000FF ) {
    UART_WriteByte(2); //invalid
    return 2;
  }

  int ret = Commands_WriteFlash256(parsed_addr, &(cmd[4]) );
  if( ret == 1 )
    UART_WriteByte(1); //success
  else
    UART_WriteByte(3); //fail
    
  return 0;
}


int HandlerReset(uint8_t* cmd)
{
  HAL_ResetCPU();
  return 1;
  (void)cmd;
}
int HandlerHalt( uint8_t* cmd)
{
  UART_WriteByte(1); //fail
  //System_AppHalted = 1;
  return 1;
  (void)cmd;
}

commandHandler Handlers[16] = {

  [1] = HandlerHalt,
  [2] = HandlerReset,
  [3]=HandlerWriteFlash256,
  [4]=HandlerReadFlash256,
  [5]=HandlerExecuteFunit,

  [15]=HandlerCheckpoint,

};

int Commands_CommandReceived(int type,uint8_t* cmd)
{
  if( Handlers[type] ){
    Handlers[type](cmd);
    return 1;
  }
  return 0;
}

int Commands_WriteFlash256(uint32_t addr, uint8_t* buf)
{
  if( !addr || !buf )
    return 2;

  uintptr_t checker = (uintptr_t) addr;
  checker&=0xFF;
  if( checker )
    return 2;

  if( Flash_Write256(addr,buf) == 1 )
    return 1;
  else
    return 3;
}

//App Checkpoint.  Do stuff
int HandlerCheckpoint(uint8_t * cmd)
{
  UART_WriteByte(15); //type 15

  uint8_t flags = cmd[4];

  if( flags & 0x2 )
  {
    //print address, network order
    UART_WriteByte(cmd[3]); 
    UART_WriteByte(cmd[2]);
    UART_WriteByte(cmd[1]);
    UART_WriteByte(cmd[0]);
  }

  if( flags & 0x01 )
  {
    ;//System_Active = true;
  }
  //print message.  ascii expected, null terminated string max 100 bytes
  if( flags & 0x08 )
  {
    uint8_t * msgbuf = (uint8_t*) & (cmd[5]);
    for(uint8_t i=0; (msgbuf[i]!=0) && (i < 100);i++)
    {
      UART_WriteByte(msgbuf[i]);
    }
    UART_WriteByte(0);//null terminator for the receive side
  }

  if( flags & 0x04 )
  {
    HAL_ResetCPU();
  }

  if( flags & 0x10 )
    HAL_LedOn(0);

  return 1;
}
