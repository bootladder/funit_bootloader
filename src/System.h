#include <stdint.h>

typedef struct ApplicationHeader{

  void * application_start;
  unsigned int length;
  unsigned int checksum;

} ApplicationHeader_t;

int System_Checkpoint(uint32_t addr, uint8_t flags, uint8_t * msg);

int System_DetectApplication(void);
int System_SetApplicationHeaderAddress(void* addr);
void System_ClearAppRAM(void);
int RunApplication(void);

int System_RunApplication(void);
int System_RunSystem(void);


extern int System_Active;
extern int System_AppHalted;
//put a pointer for the applcatio
 
#define CHECKPOINTPOINTER_SECTION __attribute__ ((section(".checkpoint_pointer"),used))
#define SYSTEM_CHECKPOINT_ADDRESS 0x1FF0
//__attribute__ ((used)) 

typedef int (*system_checkout_pointer_t)(uint32_t addr, uint8_t flags, uint8_t * msg);
