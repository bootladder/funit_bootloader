
#ifndef __PROTOCOL_H_
#define __PROTOCOL_H_

#include <stdint.h>

extern uint16_t lengths_of_types[16];

extern uint8_t Protocol_ReceivedCommandBuffer[512];
extern uint8_t Protocol_ReceivedCommandType;
extern uint8_t Protocol_ReceivedNewCommand;

#endif
