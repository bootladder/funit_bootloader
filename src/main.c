#include "halLed.h"
#include "halDelay.h"
#include "UART.h"
#include "halUart.h"
#include "System.h"
#include "sysTypes.h"
#include "Commands.h"
#include "breadcrumb.h"

static void _hardware_init(void)
{
  HAL_LedInit();

  //For the Flash functions
  PM->AHBMASK.reg |= PM_AHBMASK_NVMCTRL;
  PM->APBBMASK.reg |= PM_APBBMASK_NVMCTRL;

  __asm volatile ("cpsie i"); //enable interrupts
  HAL_UartInit(9600);
}


typedef uint8_t (funitPointer)(uint8_t b);
int main(void)
{
  _hardware_init();

  //indicate power cycle
  HAL_DelayLoops(50000);HAL_LedOn(0);   
  HAL_DelayLoops(100000);HAL_LedOff(0);   
  HAL_UartWriteByte(0x55);
  HAL_UartWriteByte(0x3C);
  HAL_UartWriteByte(0x55);

  //breadcrumb_write((uint8_t*)"hello blahblah");
  uint8_t * str = breadcrumb_getString();
  uint8_t i=0;
  while(str[i] != 0){
    HAL_UartWriteByte(str[i++]);
  }

  while(1)
  {
    HAL_UartTaskHandler();
     UART_ByteReceived_Task();
    System_RunSystem(); 
  }


}
