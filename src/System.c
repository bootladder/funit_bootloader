#include "System.h"
#include "Protocol.h"
#include "Commands.h"
#include "hton.h"
//#include <string.h>
#include "my_memcpy.h"
#include "sysTypes.h"
#include "halDelay.h"


int System_RunSystem(void)
{ 
    if( Protocol_ReceivedNewCommand )
    {
    ATOMIC_SECTION_ENTER
      Commands_CommandReceived( 
          Protocol_ReceivedCommandType, 
          Protocol_ReceivedCommandBuffer);
      Protocol_ReceivedNewCommand = 0;
    ATOMIC_SECTION_LEAVE
      return 1;
    }

  return 0;
}

