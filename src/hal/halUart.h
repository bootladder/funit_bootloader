
#ifndef _HAL_UART_H_
#define _HAL_UART_H_

/*- Includes ---------------------------------------------------------------*/
#include <stdint.h>

/*- Prototypes -------------------------------------------------------------*/
void HAL_UartInit(uint32_t baudrate);
void HAL_UartWriteByte(uint8_t byte);
void HAL_UartTaskHandler(void);

#endif // _HAL_UART_H_
