
#include <stdint.h>

int UART_Init(uint32_t baud);
int UART_WriteByte(uint8_t byte);
int UART_ByteReceived(uint8_t byte);
int UART_ByteReceived_Task(void);
