
#include "halReset.h"

#include "sysTypes.h"

void HAL_ResetCPU(void)
{
  NVIC_SystemReset();
}
