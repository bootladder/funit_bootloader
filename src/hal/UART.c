/*
 *  UART.c : UART_ByteReceived
 */
#include "UART.h"
#include "Protocol.h"
#include "halUart.h"
#include "my_memcpy.h"

#include "sysTypes.h"

//Fill up FIFO with received bytes
uint8_t uart_fifo[512];
uint16_t uart_fifo_index;

//Prepare command buffer and flag
uint8_t Protocol_ReceivedNewCommand;
uint8_t Protocol_ReceivedCommandType;
uint8_t Protocol_ReceivedCommandBuffer[512];

uint8_t receiving_a_frame = 0;
uint8_t received_frame_length = 0;
uint8_t is_byte_received  = 0;
uint8_t received_byte = 0;
uint8_t byte_received_pos = 0;

int UART_ByteReceived_Task(void)
{
  uint8_t shadowbyte ;
  if( is_byte_received) {

    ATOMIC_SECTION_ENTER
    is_byte_received  = 0;
    shadowbyte = received_byte;
    ATOMIC_SECTION_LEAVE
    
    if( receiving_a_frame )
    {
      uart_fifo[uart_fifo_index++] = shadowbyte;
      if( uart_fifo_index == 
          (lengths_of_types[Protocol_ReceivedCommandType] - 1) )//length includes the type
                                      //but type not included in buffer
      {
        my_memcpy(Protocol_ReceivedCommandBuffer,uart_fifo, uart_fifo_index);
        receiving_a_frame = 0;
        uart_fifo_index = 0;
        Protocol_ReceivedNewCommand = 1;
        return 1;
      }
      return 2;
    }
    else if( lengths_of_types[(unsigned int)shadowbyte] > 1)
    {
      receiving_a_frame = 1;
      Protocol_ReceivedCommandType = shadowbyte;
      return 2;
    }
    else if( lengths_of_types[(unsigned int)shadowbyte] == 1)
    {
      //got a single byte command
      Protocol_ReceivedNewCommand = 1;
      Protocol_ReceivedCommandType = shadowbyte;
      return 1;
    }

  }
  return 7;
}

int UART_ByteReceived(uint8_t byte)
{
  received_byte  = byte;
  byte_received_pos++;
  is_byte_received = 1;
  return 1;
}

//int UART_ByteReceived(uint8_t byte)
//{
//  if( receiving_a_frame )
//  {
//    uart_fifo[uart_fifo_index++] = byte;
//    if( uart_fifo_index == 
//        (lengths_of_types[Protocol_ReceivedCommandType] - 1) )//length includes the type
//                                    //but type not included in buffer
//    {
//      my_memcpy(Protocol_ReceivedCommandBuffer,uart_fifo, uart_fifo_index);
//      receiving_a_frame = 0;
//      uart_fifo_index = 0;
//      Protocol_ReceivedNewCommand = 1;
//      return 1;
//    }
//    return 2;
//  }
//  else if( lengths_of_types[(unsigned int)byte] == 1)
//  {
//    //got a single byte command
//    Protocol_ReceivedNewCommand = 1;
//    Protocol_ReceivedCommandType = byte;
//    return 1;
//  }
//  else if( lengths_of_types[(unsigned int)byte] > 1)
//  {
//    receiving_a_frame = 1;
//    Protocol_ReceivedCommandType = byte;
//    return 2;
//  }
//
//  return 0;
//}



int UART_WriteByte(uint8_t byte)
{
  HAL_UartWriteByte(byte); 
  return 1;
}

int UART_Init(uint32_t baud)
{
  HAL_UartInit(baud );
  return 1;
}
