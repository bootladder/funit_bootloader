
/*- Includes ---------------------------------------------------------------*/
#include <stdbool.h>
#include "sysTypes.h"
#include "halUart.h"
#include "halGpio.h"

#include "UART.h"

/*- Definitions ------------------------------------------------------------*/
#ifndef HAL_UART_TX_FIFO_SIZE
#define HAL_UART_TX_FIFO_SIZE  1000
#endif


void HAL_IrqHandlerSercom4(void);
// Ignore HAL_UART_CHANNEL setting. We always use SERCOM4.
HAL_GPIO_PIN(UART_TXD, B, 8);
HAL_GPIO_PIN(UART_RXD, B, 9);

/*- Types ------------------------------------------------------------------*/
typedef struct
{
  uint16_t  head;
  uint16_t  tail;
  uint16_t  size;
  uint16_t  bytes;
  uint8_t   *data;
} FifoBuffer_t;

/*- Variables --------------------------------------------------------------*/
static FifoBuffer_t txFifo;
static uint8_t txData[HAL_UART_TX_FIFO_SIZE+1];

static volatile bool udrEmpty;
static volatile bool newData;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
static void halUartSync(void)
{
  while (SERCOM4->USART.STATUS.bit.SYNCBUSY);
}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartInit(uint32_t baudrate)
{
  uint64_t brr = (uint64_t)65536 * (8000000 - 16 * baudrate) / 8000000;

  HAL_GPIO_UART_TXD_out();
  HAL_GPIO_UART_TXD_pmuxen();
  HAL_GPIO_UART_RXD_in();
  HAL_GPIO_UART_RXD_pmuxen();

  PORT->Group[HAL_GPIO_PORTB].PMUX[4].bit.PMUXE = 3/*D*/; // TX
  PORT->Group[HAL_GPIO_PORTB].PMUX[4].bit.PMUXO = 3/*D*/; // RX

  //Enable OSC8M, no prescaler
  SYSCTRL->OSC8M.reg |= SYSCTRL_OSC8M_ENABLE;
  SYSCTRL->OSC8M.bit.PRESC = 0;

  //Configure Generator 3, OSC8M divide by 1
  GCLK->GENDIV.reg = GCLK_GENDIV_ID(GCLK_GENCTRL_ID_GCLK3_Val) |
            GCLK_GENDIV_DIV(1);
  //Enable GCLK3
  GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(GCLK_GENCTRL_ID_GCLK3_Val) |
            GCLK_GENCTRL_GENEN              |
            GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_OSC8M_Val) ;





  PM->APBCMASK.reg |= PM_APBCMASK_SERCOM4;

  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(SERCOM4_GCLK_ID_CORE) |
      GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN(3);

  SERCOM4->USART.CTRLB.reg = SERCOM_USART_CTRLB_RXEN | SERCOM_USART_CTRLB_TXEN |
      SERCOM_USART_CTRLB_CHSIZE(0/*8 bits*/);
  halUartSync();

  SERCOM4->USART.BAUD.reg = (uint16_t)brr;
  halUartSync();

  SERCOM4->USART.CTRLA.reg = SERCOM_USART_CTRLA_ENABLE |
      SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_MODE_USART_INT_CLK |
      SERCOM_USART_CTRLA_RXPO(1/*PAD1*/) /* | SERCOM_USART_CTRLA_TXPO *//*PAD2*/;
  halUartSync();

  SERCOM4->USART.INTENSET.reg = SERCOM_USART_INTENSET_RXC;
  NVIC_EnableIRQ(SERCOM4_IRQn);

  txFifo.data = txData;
  txFifo.size = HAL_UART_TX_FIFO_SIZE;
  txFifo.bytes = 0;
  txFifo.head = 0;
  txFifo.tail = 0;

  udrEmpty = true;
  newData = false;

}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartWriteByte(uint8_t byte)
{
  if (txFifo.bytes == txFifo.size)
    return;

  txFifo.data[txFifo.tail++] = byte;
  if (txFifo.tail == txFifo.size)
    txFifo.tail = 0;
  txFifo.bytes++;
}

/*************************************************************************//**
*****************************************************************************/
void HAL_LedOn(uint8_t i);
void HAL_IrqHandlerSercom4(void)
{
  uint8_t flags = SERCOM4->USART.INTFLAG.reg;

  if (flags & SERCOM_USART_INTFLAG_DRE)
  {
    udrEmpty = true;
    SERCOM4->USART.INTENCLR.reg = SERCOM_USART_INTENCLR_DRE;
  }

  if (flags & SERCOM_USART_INTFLAG_RXC)
  {
    uint16_t status = SERCOM4->USART.STATUS.reg;
    uint8_t byte = SERCOM4->USART.DATA.reg;

    if (0 == (status & (SERCOM_USART_STATUS_BUFOVF | SERCOM_USART_STATUS_FERR |
        SERCOM_USART_STATUS_PERR)))
    {

      UART_ByteReceived(byte);

      newData = true;
    }
  }

}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartTaskHandler(void)
{
  if (txFifo.bytes && udrEmpty)
  {
    uint8_t byte;

    byte = txFifo.data[txFifo.head++];
    if (txFifo.head == txFifo.size)
      txFifo.head = 0;
    txFifo.bytes--;

    ATOMIC_SECTION_ENTER
      SERCOM4->USART.DATA.reg = byte;
      SERCOM4->USART.INTENSET.reg = SERCOM_USART_INTENSET_DRE;
      udrEmpty = false;
    ATOMIC_SECTION_LEAVE
  }

}
