#include "sysTypes.h"
#include "halFlash.h"
#include "my_memcpy.h"

#define FLASH_PAGE_SIZE 64
#define PAGES_IN_ERASE_BLOCK  4
#define ERASE_BLOCK_SIZE      (FLASH_PAGE_SIZE * PAGES_IN_ERASE_BLOCK)

static void flash_write_page(void);

static uint32_t flash_offset = 0;
static uint32_t flash_data[(ERASE_BLOCK_SIZE / sizeof(uint32_t))];


int Flash_Write256(uint32_t addr, uint8_t* buf)
{
  if( !addr || !buf )
    return 0;

  //write 4 pages (erases included in the function)
    flash_offset = addr;
    my_memcpy(flash_data,buf,256);
    flash_write_page();
  
  return 1;
}

//-----------------------------------------------------------------------------
static void flash_write_page(void)
{
  uint32_t *offset = (uint32_t *)flash_offset;
  uint32_t *data = flash_data;

  NVMCTRL->CTRLB.reg = 0;
  NVMCTRL->ADDR.reg = flash_offset >> 1;
  NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_ER;

  while (0 == NVMCTRL->INTFLAG.bit.READY)
    ; //i2c_task();

  for (int page = 0; page < PAGES_IN_ERASE_BLOCK; page++)
  {
    __DSB();
    __ISB();

    for (uint32_t i = 0; i < FLASH_PAGE_SIZE / sizeof(uint32_t); i++)
      *offset++ = *data++;

    __DSB();
    __ISB();


    while (0 == NVMCTRL->INTFLAG.bit.READY)
      ;//i2c_task();
  }

  flash_offset = 0;
}

