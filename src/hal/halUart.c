
#ifdef USING_SERCOM1
  #define SERCOMBASEPTR SERCOM1
  #define SERCOM_PM_APBCMASK PM_APBCMASK_SERCOM1
  #define SERCOM_IRQn_NUMBER SERCOM1_IRQn
  #define SERCOM_GCLK_ID_CORE_NUMBER SERCOM1_GCLK_ID_CORE
#endif
#ifdef USING_SERCOM0
  #define SERCOMBASEPTR SERCOM0
  #define SERCOM_PM_APBCMASK PM_APBCMASK_SERCOM0
  #define SERCOM_IRQn_NUMBER SERCOM0_IRQn
  #define SERCOM_GCLK_ID_CORE_NUMBER SERCOM0_GCLK_ID_CORE
#endif
#ifdef USING_SERCOM3

  #define SERCOMBASEPTR SERCOM3
#endif

/*- Includes ---------------------------------------------------------------*/
#include <stdbool.h>
#include "sysTypes.h"
#include "halUart.h"
#include "halGpio.h"
#include "breadcrumb.h"

#include "UART.h"

/*- Definitions ------------------------------------------------------------*/
#ifndef HAL_UART_TX_FIFO_SIZE
#define HAL_UART_TX_FIFO_SIZE  1000
#endif


void HAL_IrqHandlerSercom0(void);
// Ignore HAL_UART_CHANNEL setting. We always use SERCOM4.
HAL_GPIO_PIN(UART_TXD, A, 4);
HAL_GPIO_PIN(UART_RXD, A, 5);

/*- Types ------------------------------------------------------------------*/
typedef struct
{
  uint16_t  head;
  uint16_t  tail;
  uint16_t  size;
  uint16_t  bytes;
  uint8_t   *data;
} FifoBuffer_t;

/*- Variables --------------------------------------------------------------*/
static FifoBuffer_t txFifo;
static uint8_t txData[HAL_UART_TX_FIFO_SIZE+1];

static volatile bool udrEmpty;
static volatile bool newData;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
static void halUartSync(void)
{
  //while (SERCOM1->USART.STATUS.bit.SYNCBUSY);
  //while (SERCOMBASEPTR->USART.SYNCBUSY.reg);
}

/*************************************************************************//**
*****************************************************************************/
#define REGISTERVAL_FOR_115200 (uint64_t)(65536ull * (8000000ull - 16ull * 115200ull) / 8000000ull ) 
#define REGISTERVAL_FOR_38400 (uint64_t)(65536ull * (8000000ull - 16ull * 38400ull) / 8000000ull ) 
#define REGISTERVAL_FOR_9600   (uint64_t)(65536ull * (8000000ull - 16ull * 9600ull) / 8000000ull ) 
void HAL_UartInit(uint32_t baudrate)
{
  uint64_t brr;
  if( baudrate == 115200 )
    brr = REGISTERVAL_FOR_115200;
  else if( baudrate == 38400 )
    brr = REGISTERVAL_FOR_38400 ;
  else if( baudrate == 9600)
    brr = REGISTERVAL_FOR_9600   ;
  else 
    brr = REGISTERVAL_FOR_115200;

  HAL_GPIO_UART_TXD_out();
  HAL_GPIO_UART_TXD_pmuxen();
  HAL_GPIO_UART_RXD_in();
  HAL_GPIO_UART_RXD_pmuxen();

  PORT->Group[HAL_GPIO_PORTA].PMUX[2].bit.PMUXE = 2/*C*/; // TX
  PORT->Group[HAL_GPIO_PORTA].PMUX[2].bit.PMUXO = 2/*C*/; // RX

  //Enable OSC8M, no prescaler
  SYSCTRL->OSC8M.reg |= SYSCTRL_OSC8M_ENABLE;
  SYSCTRL->OSC8M.bit.PRESC = 0;

  //Configure Generator 3, OSC8M divide by 1
  //GCLK->GENDIV.reg = GCLK_GENDIV_ID(GCLK_GENCTRL_ID_GCLK3_Val) |
  //          GCLK_GENDIV_DIV(1);
  GCLK->GENDIV.reg = GCLK_GENDIV_ID(3) |
            GCLK_GENDIV_DIV(1);
  //Enable GCLK3
  //GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(GCLK_GENCTRL_ID_GCLK3_Val) |
  GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(3) |
            GCLK_GENCTRL_GENEN              |
            GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_OSC8M_Val) ;





  PM->APBCMASK.reg |= SERCOM_PM_APBCMASK; 

  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(SERCOM_GCLK_ID_CORE_NUMBER) |
      GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN(3);

  SERCOMBASEPTR->USART.CTRLB.reg = SERCOM_USART_CTRLB_RXEN | SERCOM_USART_CTRLB_TXEN |
      SERCOM_USART_CTRLB_CHSIZE(0/*8 bits*/);
  halUartSync();

  SERCOMBASEPTR->USART.BAUD.reg = (uint16_t)brr;
  halUartSync();

  SERCOMBASEPTR->USART.CTRLA.reg = SERCOM_USART_CTRLA_ENABLE |
      SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_MODE_USART_INT_CLK |
      SERCOM_USART_CTRLA_RXPO(3/*PAD3*/)  | SERCOM_USART_CTRLA_TXPO(1) /*PAD2*/;
  halUartSync();

  SERCOMBASEPTR->USART.INTENSET.reg = SERCOM_USART_INTENSET_RXC;
  NVIC_EnableIRQ(SERCOM_IRQn_NUMBER);

  txFifo.data = txData;
  txFifo.size = HAL_UART_TX_FIFO_SIZE;
  txFifo.bytes = 0;
  txFifo.head = 0;
  txFifo.tail = 0;

  udrEmpty = true;
  newData = false;

}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartWriteByte(uint8_t byte)
{
  if (txFifo.bytes == txFifo.size)
    return;

  txFifo.data[txFifo.tail++] = byte;
  if (txFifo.tail == txFifo.size)
    txFifo.tail = 0;
  txFifo.bytes++;
}

/*************************************************************************//**
*****************************************************************************/
void HAL_LedOn(uint8_t i);
void irq_handler_sercom0(void)
//void HAL_IrqHandlerSercom1(void)
{
  uint8_t flags = SERCOMBASEPTR->USART.INTFLAG.reg;

  if (flags & SERCOM_USART_INTFLAG_DRE)
  {
    udrEmpty = true;
    SERCOMBASEPTR->USART.INTENCLR.reg = SERCOM_USART_INTENCLR_DRE;
  }

  if (flags & SERCOM_USART_INTFLAG_RXC)
  {
    uint16_t status = SERCOMBASEPTR->USART.STATUS.reg;
    uint8_t byte = SERCOMBASEPTR->USART.DATA.reg;

    if (0 == (status & (SERCOM_USART_STATUS_BUFOVF | SERCOM_USART_STATUS_FERR |
        SERCOM_USART_STATUS_PERR)))
    {

      UART_ByteReceived(byte);

      newData = true;
    }
    else if ( status & SERCOM_USART_STATUS_BUFOVF) {
    ATOMIC_SECTION_ENTER
      breadcrumb_write((uint8_t*)"BUF OVRF");
    ATOMIC_SECTION_LEAVE
    }
    else if ( status & SERCOM_USART_STATUS_FERR) {
    ATOMIC_SECTION_ENTER
      breadcrumb_write((uint8_t*)"FRAME ER");
    ATOMIC_SECTION_LEAVE
    }
    else if ( status & SERCOM_USART_STATUS_PERR) {
    ATOMIC_SECTION_ENTER
      breadcrumb_write((uint8_t*)"PARITY? ERR");
    ATOMIC_SECTION_LEAVE
    }
  }

}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartTaskHandler(void)
{
  if (txFifo.bytes && udrEmpty)
  {
    uint8_t byte;

    byte = txFifo.data[txFifo.head++];
    if (txFifo.head == txFifo.size)
      txFifo.head = 0;
    txFifo.bytes--;

    ATOMIC_SECTION_ENTER
      SERCOMBASEPTR->USART.DATA.reg = byte;
      SERCOMBASEPTR->USART.INTENSET.reg = SERCOM_USART_INTENSET_DRE;
      udrEmpty = false;
    ATOMIC_SECTION_LEAVE
  }

}
