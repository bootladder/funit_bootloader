#ifndef _SYS_TYPES_H_
#define _SYS_TYPES_H_

#include <stdint.h>

#ifdef SAMD10
  #include "samd10c14a.h"
#else
  #include "samd20j18.h"
#endif

#define SYS_MCU_ARCH_CORTEX_M

#if defined(SYS_MCU_ARCH_CORTEX_M)
    #define PRAGMA(x)

    #define PACK __attribute__ ((packed))

    #define INLINE static inline __attribute__ ((always_inline))

    #define SYS_EnableInterrupts() __asm volatile ("cpsie i");

    #define ATOMIC_SECTION_ENTER   { register uint32_t __atomic; \
                                     __asm volatile ("mrs %0, primask" : "=r" (__atomic) ); \
                                     __asm volatile ("cpsid i");
    #define ATOMIC_SECTION_LEAVE   __asm volatile ("msr primask, %0" : : "r" (__atomic) ); }

#endif

#endif // _SYS_TYPES_H_




