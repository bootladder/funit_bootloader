#include "breadcrumb.h"

BREADCRUMB_SECTION uint8_t _breadcrumb_string[16];

void breadcrumb_write(uint8_t * string)
{
  uint8_t c = 0;
  while(string[c] != 0){
    _breadcrumb_string[c]=string[c];
    c++;
  }
  _breadcrumb_string[c] = 0;
}

uint8_t * breadcrumb_getString(void)
{
  return _breadcrumb_string;
}
