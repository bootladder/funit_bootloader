#################
#  PARAMETERS
#
PATH_TO_LINKERSCRIPT = ./samd10c14.ld
#PATH_TO_LINKERSCRIPT = ./atsamd20j18.ld
#PATH_TO_CMSIS_HEADERS = ../samd20_cmsis_headers 
PATH_TO_CMSIS_HEADERS = ../samd10_cmsis_headers 
PATH_TO_ARM_HEADERS   = ../arm_cmsis_headers 
PATH_TO_STARTUP_CODE = src/hal/startup_samd10.c
OPTIONAL_COMPILER_DEFINES = -DSAMD10 -DUSING_SERCOM0
#
#

##################
# PATHS
#
CLEANUP = rm -f
MKDIR = mkdir -p

UNITY_ROOT=./link_to_unity_root
HOST_C_COMPILER=gcc
HOST_AR=ar
CROSS_C_COMPILER=arm-none-eabi-gcc
CROSS_OBJDUMP =arm-none-eabi-objdump
CROSS_AR=arm-none-eabi-ar
OBJCOPY = arm-none-eabi-objcopy
SIZE = arm-none-eabi-size

##################
# FLAGS
#
CFLAGS = -std=c99
CFLAGS += -Wall -Werror
CFLAGS += -Wextra
CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition
CFLAGS += -Wmissing-prototypes
CFLAGS += -Wmissing-declarations
CFLAGS += -DUNITY_FIXTURES

CROSS_CFLAGS += -W -Wall -Werror --std=gnu99 -Os 
CROSS_CFLAGS += -Wl,--gc-sections
CROSS_CFLAGS += -Wl,-Map=output.map
CROSS_CFLAGS += -fdata-sections -ffunction-sections
CROSS_CFLAGS += -fno-asynchronous-unwind-tables
CROSS_CFLAGS += -funsigned-char -funsigned-bitfields
CROSS_CFLAGS += -mcpu=cortex-m0plus -mthumb
CROSS_CFLAGS += -nostdlib -nostartfiles

CROSS_LDFLAGS += -mcpu=cortex-m0plus -mthumb
CROSS_LDFLAGS += -Wl,--script=$(PATH_TO_LINKERSCRIPT) 

##################
# TARGETS
#
TARGET_PRODUCTION_NAME = funit_bootloader$(OPT_TARGET_EXTENSION)
TARGET_HOST_TEST = funit_All_Tests.out
TARGET_HOST_PRODUCTION_LIB = libfunit_Host_Production_Lib.a
TARGET_CROSS_TEST = funit_All_Tests.elf
TARGET_CROSS_TEST_BIN = funit_All_Tests.bin

TARGET_CROSS_PRODUCTION = funit_bootloader.elf
TARGET_CROSS_PRODUCTION_BIN = funit_bootloader.bin
TARGET_CROSS_PRODUCTION_LIB = libfunit_Cross_Production_Lib.a

LIBRARY_ARGUMENTS_INCLUDE_TARGET_HOST_PRODUCTION_LIB = -L. -lfunit_Host_Production_Lib
LIBRARY_ARGUMENTS_INCLUDE_TARGET_CROSS_PRODUCTION_LIB = -L. -lfunit_Cross_Production_Lib

##################
# SOURCES
#
SRC_FILES_PRODUCTION=\
  src/Commands.c \
  src/System.c \
  src/my_memcpy.c \
  src/Protocol.c \
  src/hal/UART.c \
  src/hal/halDelay.c \
  src/breadcrumb.c \

SRC_FILES_PRODUCTION_MAIN=\
	src/main.c \

SRC_FILES_PRODUCTION_CROSSTARGET_ONLY=\
  src/hal/halUart.c \
  src/hal/halFlash.c \
  src/hal/halReset.c \
  $(PATH_TO_STARTUP_CODE) \

SRC_FILES_TEST = \
  $(UNITY_ROOT)/src/unity.c \
  $(UNITY_ROOT)/extras/fixture/src/unity_fixture.c \
	\
  test/test_runners/CommandsTest_Runner.c \
  test/test_runners/UARTTest_Runner.c \
  test/test_runners/all_tests.c \
  test/CommandsTest.c \
  test/hal/UARTTest.c \
    \

  #test/test_runners/SystemTest_Runner.c \

  #test/SystemTest.c \

SRC_FILES_MOCK = \
	mock/MockhalFlash.c \
	mock/MockhalUart.c \
	mock/MockhalReset.c \
	mock/MockhalLed.c \
    \

##################
# GCC SWITCHES
#
INC_DIRS=-Isrc -Isrc/hal -Isrc \
						 -I$(UNITY_ROOT)/src -I$(UNITY_ROOT)/extras/fixture/src

INC_DIRS_CROSS = -I$(PATH_TO_CMSIS_HEADERS) -I$(PATH_TO_ARM_HEADERS)
INC_DIRS_MOCK=-Imock
SYMBOLS=


##########################################################################3333

##################
# MAKE TARGETS
#
all: production_cross clean




######################################################
# test_host
#		create test.out, run it on host
#
test_host: $(TARGET_HOST_TEST) clean 
	@echo -e '\n'$@'\n' Running Test Executable on Host
	./$(TARGET_HOST_TEST) -v

$(TARGET_HOST_TEST): $(SRC_FILES_TEST) $(TARGET_HOST_PRODUCTION_LIB)
	@echo  -e '\n'$@'\n' Building Test Executable for Host
	@echo  -e '\n'$@'\n' deleting object files rm -rf *.o
	@rm -rf *.o
	@echo  -e '\n'$@'\n' Creating Object files
	$(HOST_C_COMPILER) $(CFLAGS) $(INC_DIRS) \
			$(INC_DIRS_MOCK) \
			$(SRC_FILES_TEST) $(SRC_FILES_MOCK) \
			-c

	@echo  -e '\n'$@'\n' Linking together the Test Executable
	$(HOST_C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) \
				-o $(TARGET_HOST_TEST) \
			*.o \
			$(LIBRARY_ARGUMENTS_INCLUDE_TARGET_HOST_PRODUCTION_LIB) \


$(TARGET_HOST_PRODUCTION_LIB): production_host_objects
	@echo -e '\n'$@'\n'  Building production library for testing
	@echo ar rcs  $(TARGET_HOST_PRODUCTION_LIB) *.o 
	@$(HOST_AR) rcs  $(TARGET_HOST_PRODUCTION_LIB) *.o 
	#create the object files for library		

production_host_objects: $(SRC_FILES1) 
	@echo -e '\n'$@'\n' Compiling tested production objects for Host
	$(HOST_C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION)  \
				 -c
######################################################


######################################################
# production_cross
# 		CROSS COMPILES:  ONLY CREATE THE PRODUCTION EXECUTABLE

production_cross: $(SRC_FILES1)
	@echo Building production executable Cross Compiled
	$(CROSS_C_COMPILER) $(CROSS_CFLAGS) $(INC_DIRS) $(OPTIONAL_COMPILER_DEFINES) $(SYMBOLS) \
      $(INC_DIRS_CROSS) \
			$(SRC_FILES_PRODUCTION) $(SRC_FILES_PRODUCTION_MAIN) \
				$(SRC_FILES_PRODUCTION_CROSSTARGET_ONLY) \
			$(CROSS_LDFLAGS) \
				 -o $(TARGET_CROSS_PRODUCTION)

	@echo $(OBJCOPY) -O binary -R .eeprom $(TARGET_CROSS_PRODUCTION) $(TARGET_CROSS_BIN)
	@$(OBJCOPY) -O binary -R .eeprom $(TARGET_CROSS_PRODUCTION) $(TARGET_CROSS_PRODUCTION_BIN)
	@$(SIZE) $(TARGET_CROSS_PRODUCTION)
	$(CROSS_OBJDUMP) -h -S "$(TARGET_PRODUCTION_NAME).elf" > "$(TARGET_PRODUCTION_NAME).lss"
	
clean:
	$(CLEANUP) *.o

deploy:
	@sudo edbg -t atmel_cm0p -p -f $(TARGET_CROSS_PRODUCTION_BIN)
